// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "Math/UnrealMathUtility.h"
#include "SnakeBase.h"

// Sets default values
AFoodBase::AFoodBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
}

// Called when the game starts or when spawned
void AFoodBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFoodBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFoodBase::CreateFoodActor()
{
	bool bSpeedFood = FMath::RandBool();
	FVector NewLocation(FMath::RandRange(-420, 420), FMath::RandRange(-420, 420), 0);
	FTransform NewTransform(NewLocation);
	if (bSpeedFood)
	{
		GetWorld()->SpawnActor<AFoodBase>(FoodSpeedActorClass, NewTransform);
	}
	else
	{
		GetWorld()->SpawnActor<AFoodBase>(FoodSimpleActorClass, NewTransform);
	}
}

void AFoodBase::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			Snake->Score += 1;
			if (NameActorClass == "FoodSpeed")
			{
				Snake->SetNewMovementSpeed();
			}
			CreateFoodActor();
			Destroy();
		}
	}
}
