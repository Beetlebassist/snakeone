// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "FoodBase.generated.h"

class UStaticMeshComponent;

UCLASS()
class SNAKEONE_API AFoodBase : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFoodBase();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
	UStaticMeshComponent* MeshComponent;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFoodBase> FoodSimpleActorClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFoodBase> FoodSpeedActorClass;

	UPROPERTY(EditDefaultsOnly)
	FString NameActorClass;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void CreateFoodActor();

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

};
