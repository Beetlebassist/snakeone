// Copyright Epic Games, Inc. All Rights Reserved.

#include "SnakeOne.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SnakeOne, "SnakeOne" );
