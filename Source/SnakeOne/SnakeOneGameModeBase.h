// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeOneGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEONE_API ASnakeOneGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
